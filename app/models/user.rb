class User < ApplicationRecord
    before_save { self.email = email.downcase }
    validates :display_name, presence: true, length: { maximum: 55 }
    validates :email, presence: true, length: { maximum: 55 }
    validates :password, presence: true, length: { minimum: 6 }
    validates :email, uniqueness: true
    has_secure_password
  end
